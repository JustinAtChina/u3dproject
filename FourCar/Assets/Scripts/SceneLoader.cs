﻿using UnityEngine;
using System.Collections;
using System.IO;

using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    public string AssetbundleURL;
    public GameObject SceneRoot;

    void Start()
    {
        Debug.Log("##LoadSence!##");
        AssetbundleURL = "file://" + Application.streamingAssetsPath+"/start0.assetbundle";
        StartCoroutine(LoadStartSence(AssetbundleURL));
    }

    void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }


    IEnumerator LoadStartSence(string name)
    {
        WWW www = new WWW(name);
        yield return www;
        AssetBundle bundles = www.assetBundle;
        var objs = bundles.LoadAllAssets<GameObject>();
        foreach (var o in objs)
        {
            GameObject obj = Instantiate(o) as GameObject;
            obj.transform.SetParent(SceneRoot.transform);
        }
    }
}
