﻿using UnityEngine;
using UnityStandardAssets.Network;
using System.Collections;
using UnityEngine.Networking;

public class MyNetworkLobbyHook : LobbyHook
{
    public override void OnLobbyServerSceneLoadedForPlayer(NetworkManager manager, GameObject lobbyPlayer, GameObject gamePlayer)
    {
        /*
        LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();
        NetworkSpaceship spaceship = gamePlayer.GetComponent<NetworkSpaceship>();

        spaceship.name = lobby.name;
        spaceship.color = lobby.playerColor;
        spaceship.score = 0;
        spaceship.lifeCount = 3;
        */

        LobbyPlayer lobby = lobbyPlayer.GetComponent<LobbyPlayer>();
        NetWorkCar car = gamePlayer.GetComponent<NetWorkCar>();

        car.color = lobby.playerColor;
        car.playerName = lobby.playerName;
    }
}
