﻿using UnityEngine;
using System.Collections;

namespace UI_ButtonsType 
{
    public enum ButtonsType
    {
        BUTTON_TYPE_BOTTOMBOARD,
        BUTTON_TYPE_WHEEL
    }
}
