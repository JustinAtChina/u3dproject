﻿using UnityEngine;
using System.Collections;
using UI_ButtonsType;

public class PartsItem : MonoBehaviour
{
    //public   ButtonsType m_type;
    //public void ButtonsType()
    //{
       
    //    switch (m_type)
    //    {
    //        case UI_ButtonsType.ButtonsType.BUTTON_TYPE_BOTTOMBOARD:
    //        {
    //            Debug.Log("BUTTON_TYPE_BOTTOMBOARD");
    //            ModuleObject.Instance.CreateBottomBoard();
    //        }
    //            break;
    //        case UI_ButtonsType.ButtonsType.BUTTON_TYPE_WHEEL:
    //        {
    //            Debug.Log("BUTTON_TYPE_WHEEL");
    //        }
    //            break;
    //        default:
    //            break;
    //    }
    //}

    //返回到游戏大厅
    public void ReturnToLobby()
    {
        Debug.Log("BUTTON_TYPE_ReturnTOLOBBY");
        Application.LoadLevel("start");
    }


    //底板
    public void BottomBoardButton()
    {
        Debug.Log("BUTTON_TYPE_BOTTOMBOARD");
        ModuleManager.Instance.addModule(ModuleStrDef.MODULE.STR_MODULE_CUBE_SMALL);
        //ModuleObject.Instance.CreateBottomBoard();
    }
    //轮子
    public void WheelButton()
    {
        Debug.Log("BUTTON_TYPE_WHEEL");
        ModuleManager.Instance.addModule(ModuleStrDef.MODULE.STR_MODULE_WHEEL_POWER);
    }

    //圆柱体
    public void CylinderButton()
    {
        Debug.Log("BUTTON_TYPE_CYLINDER");
        ModuleManager.Instance.addModule(ModuleStrDef.MODULE.STR_MODULE_CYLINDER);
    }


}
