﻿using UnityEngine;

/**  
 *@author       GhostXi
 *@version      0.0.0.2
 *@datetime     05/30/2016
 *@unityversion 5.0+
**/
public class CameraViewTransform : MonoBehaviour
{
    private Transform target;
    public Vector3 initAngle = new Vector3(30, 40, 0);

    public float xSpeed = 200;
    public float ySpeed = 200;
    private float mSpeed = 10;
    public float yMinLimit = -50;
    public float yMaxLimit = 50;

    private float distance = 2;
    private float minDistance = 2;
    private float maxDistance = 10;

    public bool needDamping = true;
    private float damping = 5.0f;

    private float m_x = 0.0f;
    private float m_y = 0.0f;
    private bool m_isCanCtrl = false;
    private Vector3 m_mouseposition;

    // Use this for initialization
    void Start()
    {
        
    }

    /// <summary>初始化相机观察对象,并返回相机与对象的距离</summary>
    public float init(Transform initTarget)
    {
        this.transform.position = Vector3.zero;
        this.transform.rotation = Quaternion.identity;

        Bounds bounds = ToCenter(initTarget);
        float maxLenght = Mathf.Max(bounds.size.x, bounds.size.y, bounds.size.z);
        float fDis = maxLenght / Mathf.Tan(Camera.main.fieldOfView / 15f * Mathf.Deg2Rad);
        distance = fDis;

        minDistance = distance / 5;
        maxDistance = distance * 2;
        mSpeed = distance / 2;

        target = initTarget;
        m_isCanCtrl = false;
        calculatePosition(false);

        return distance;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (target == null) return;

        if (Input.GetMouseButtonDown(1))
        {
            m_mouseposition = Input.mousePosition;
            //            _Log.Info(null, "Input.mousePosition, position:{0}", m_mouseposition);
        }

        if (m_isCanCtrl) calculatePosition(needDamping);
        else
        {
            if (Input.GetMouseButton(1) || Input.GetAxis("Mouse ScrollWheel")!=null)
            {
                Vector3 angles = transform.eulerAngles;
                m_x = angles.y;
                m_y = angles.x;
                m_isCanCtrl = true;
                return;
            }

            Quaternion rotation = Quaternion.Euler(initAngle.x, 360 - initAngle.y, initAngle.z);
            Vector3 disVector = new Vector3(0.0f, 0.0f, -distance);
            Vector3 position = rotation * disVector + target.position;
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * damping * 0.5f);
            transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * damping * 0.5f);

            if (Vector3.Distance(transform.localEulerAngles, initAngle) <= 1)
            {
                m_isCanCtrl = true;
                Vector3 angles = transform.eulerAngles;
                m_x = angles.y;
                m_y = angles.x;
            }
        }
    }

    private void calculatePosition(bool damp)
    {
        if (Input.GetMouseButton(1))
        {
            Vector3 mousepos = Input.mousePosition;
            float fDeltaX = mousepos.x - m_mouseposition.x;
            float fDeltaY = mousepos.y - m_mouseposition.y;
            m_mouseposition = mousepos;

            m_x += Input.GetAxis("Mouse X") * xSpeed * 0.02f;
            m_y -= Input.GetAxis("Mouse Y") * ySpeed * 0.02f;

            m_y = ClampAngle(m_y, yMinLimit, yMaxLimit);
        }
        distance -= Input.GetAxis("Mouse ScrollWheel") * mSpeed;
        distance = Mathf.Clamp(distance, minDistance, maxDistance);
        Quaternion rotation = Quaternion.Euler(m_y, m_x, 0.0f);
        Vector3 disVector = new Vector3(0.0f, 0.0f, -distance);
        Vector3 position = rotation * disVector + target.position;

        if (damp)
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, Time.deltaTime * damping);
            transform.position = Vector3.Lerp(transform.position, position, Time.deltaTime * damping);
        }
        else
        {
            transform.rotation = rotation;
            transform.position = position;
        }
    }

    static float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360) angle += 360;
        if (angle > 360) angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }

    static Bounds ToCenter(Transform parent)
    {
        Vector3 postion = parent.position;
        Quaternion rotation = parent.rotation;
        Vector3 scale = parent.localScale;
        parent.position = Vector3.zero;
        parent.rotation = Quaternion.Euler(Vector3.zero);
        parent.localScale = Vector3.one;

        Vector3 center = Vector3.zero;
        Renderer[] renders = parent.GetComponentsInChildren<Renderer>();
        foreach (Renderer child in renders)
        {
            center += child.bounds.center;
        }
        center /= parent.GetComponentsInChildren<Transform>().Length;
        Bounds bounds = new Bounds(center, Vector3.zero);
        foreach (Renderer child in renders)
        {
            bounds.Encapsulate(child.bounds);
        }

        parent.position = postion;
        parent.rotation = rotation;
        parent.localScale = scale;

        foreach (Transform t in parent)
        {
            t.position = t.position - bounds.center;
        }

        return bounds;
    }
}