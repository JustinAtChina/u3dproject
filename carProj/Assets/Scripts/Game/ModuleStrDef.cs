﻿using UnityEngine;
using System.Collections;

public class ModuleStrDef : MonoBehaviour {

    //存放所有模块的预置字符串
    public enum MODULE
    {
        STR_MODULE_CUBE_SMALL,//小方块
        STR_MODULE_WHEEL_POWER,//动力轮
        STR_MODULE_CYLINDER,//圆柱体
    }

    //获得对应 的预置体
    public static GameObject GetPrefab(MODULE module)
    {
        string path = null;
        switch(module)
        {
            case MODULE.STR_MODULE_CUBE_SMALL:
                path = "Prefabs/EnergyCube";
                break;
            case MODULE.STR_MODULE_WHEEL_POWER:
                path = "Prefabs/Wheel_Power";
                break;
            case MODULE.STR_MODULE_CYLINDER:
                path = "Prefabs/Module_Cylinder";
                break;
        }

        if(path==null)
        {
            Debug.LogError("找不到对应的预置体" + module);
            return null;
        }
        return Resources.Load<GameObject>(path);
    }
}
