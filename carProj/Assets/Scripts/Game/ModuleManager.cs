﻿using UnityEngine;
using System.Collections;


public class ModuleManager : MonoBehaviour
{

    public static ModuleManager Instance;
    private Transform m_SetAct;
    public bool isAdding = false;
    public static bool hasPreShow = false;
    GameObject ModuleAdd;
    [SerializeField]
    GameObject GamePlayer;
    private float spawnOffset;
    private Vector3 preShowPos;//预览生成位置
    private Vector3 lastPreShowPos;//上次预览的位置
    private Color originColor;


    void Awake()
    {
        Instance = this;
        m_SetAct = gameObject.GetComponent<Transform>();
        GamePlayer = GameObject.Find("GamePlayer");
        if (GamePlayer == null)
        {
            Debug.LogError("主模块找不到");
        }
    }

    public void addModule(ModuleStrDef.MODULE module)
    {
        //销毁之前的预置体
        if (isAdding)
        {
            Destroy(ModuleAdd);
            isAdding = false;
        }

        ModuleAdd = ModuleStrDef.GetPrefab(module);//获取预置
        if (ModuleAdd != null)
        {
            ModuleAdd = Instantiate(ModuleAdd);
            ModuleAdd.SetActive(false);
            ModuleAdd.transform.SetParent(GamePlayer.transform);
            originColor = ModuleAdd.GetComponent<Renderer>().material.color;
            //设置半透明

            isAdding = true;
        }
    }

    void Update()
    {
        if (isAdding)
        {
            DetectAddPlace();

            //生产添加物体
            if (hasPreShow && Input.GetMouseButtonDown(0))
            {
                addFinalModule();
                m_SetAct.gameObject.SetActive(false);
                //测试用的
                if (ModuleAdd.name == "EnergyCube(Clone)")
                {
                    ModuleAdd.transform.FindChild("point1").gameObject.SetActive(false);
                }
                else
                {
                    ModuleAdd.transform.FindChild("spawn1").gameObject.SetActive(false);
                }
            }
            else if (!hasPreShow && Input.GetMouseButtonDown(0))
            {
                ModuleAdd.SetActive(false);
            }
        }
    }

    //生产最终的模块
    void addFinalModule()
    {
        ModuleAdd.GetComponent<Renderer>().material.color = originColor;
        ModuleAdd.SetActive(true);
        //ModuleAdd.GetComponent<BoxCollider>().enabled = true;
        isAdding = false;
        hasPreShow = false;
    }

    //检测添加的位置
    void DetectAddPlace()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.tag == "Player" && hit.transform.position != ModuleAdd.transform.position)
            {
                if (!hasPreShow)
                {
                    PreShow(hit);
                }
            }
            if (hit.transform.tag != "Player" || (hit.transform.position != lastPreShowPos && hit.transform.position != ModuleAdd.transform.position))
            {
                if (hasPreShow)
                {
                    CanclePreShow();
                }
            }
        }
        else if (hasPreShow)
        {
            CanclePreShow();
        }
    }

    //预览加上去的样子
    void PreShow(RaycastHit hit)
    {
        if (!hasAddPoint(hit))
            return;
        Debug.Log("生产预览");
        lastPreShowPos = hit.transform.position;
        ModuleAdd.transform.position = preShowPos;
        ModuleRotate(hit.normal);
        /*设置透明度
         * Color a= ModuleAdd.GetComponent<Renderer>().material.color;
        a.a = 0.5f;
        ModuleAdd.GetComponent<Renderer>().material.SetColor("a",new Color(0,0,0,0.5f));
        */
        ModuleAdd.GetComponent<Renderer>().material.color = new Color(1, 1, 1);
        ModuleAdd.SetActive(true);
        hasPreShow = true;
    }

    //当鼠标移出可添加范围，取消预览
    void CanclePreShow()
    {
        Debug.Log("当鼠标移出可添加范围，取消预览");
        ModuleAdd.SetActive(false);
        hasPreShow = false;
    }

    //计算放置的位置
    bool hasAddPoint(RaycastHit hit)
    {
        //查找该面是否有接触点，否则不能添加
        Vector3 touchPoint = hit.point;
        bool hasPoint = false;
        foreach (Transform go in hit.transform)
        {
            Debug.Log(go.transform.name + "---" + go.transform.position);
            if (isTargetPoint(hit, go.transform.position))
            {
                hasPoint = true;
                touchPoint = go.transform.position;
                Debug.Log(go.transform.name + "--##--" + go.transform.position);
                m_SetAct = go;
                break;
            }
        }
        spawnOffset = ModuleAdd.GetComponent<ModuleInspector>().spawnOffset;
        preShowPos = touchPoint + hit.normal * spawnOffset;
        return hasPoint;
    }


    //判断改点是否为接触面上的目标点
    bool isTargetPoint(RaycastHit hit, Vector3 testPoint)
    {
        float threshold = 0.1f;
        float t1 = hit.point.x * hit.normal.x + hit.point.y * hit.normal.y + hit.point.z * hit.normal.z;
        float t2 = testPoint.x * hit.normal.x + testPoint.y * hit.normal.y + testPoint.z * hit.normal.z;
        if (Mathf.Abs(t1 - t2) < threshold)
        {
            return true;
        }
        return false;
    }


    //测试检测到的面
    void testFace(RaycastHit hit)
    {
        Vector3 MyNormal = hit.normal;
        MyNormal = hit.transform.TransformDirection(MyNormal);

        if (MyNormal == hit.transform.up)
        {
            Debug.Log("#######   Top!  ########");
        }
        if (MyNormal == -hit.transform.up)
        {
            Debug.Log("#######   Bottom!  ########");
        }
        if (MyNormal == hit.transform.forward)
        {
            Debug.Log("#######  Forward!  ########");
        }
        if (MyNormal == -hit.transform.forward)
        {
            Debug.Log("#######   Back!  ########");
        }
        if (MyNormal == hit.transform.right)
        {
            Debug.Log("#######   Right!  ########");
        }
        if (MyNormal == -hit.transform.right)
        {
            Debug.Log("#######   Left!  ########");
        }
    }

    //module旋转
    void ModuleRotate(Vector3 hit)
    {
        if (ModuleAdd != null)
        {
            Vector3 v;
            v.x = hit.z * (-90) + hit.y * (-180);
            v.y = 0;
            v.z = hit.x * (-90);
            ModuleAdd.transform.rotation = Quaternion.Euler(v);
        }
    }
}
