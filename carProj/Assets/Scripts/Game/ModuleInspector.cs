﻿using UnityEngine;
using System.Collections;

//模块的属性
public class ModuleInspector : MonoBehaviour {

    /*
     1.模块默认旋转0，0，0
     2.且最上面的面为默认添加面
     3.为了降低难度，所有面的法线与轴平行
     4.
     */

    /*
     添加新模块：设置boxcollider,tagname,spawn,spawnoffset,moduelinspector,modulestrdef/
    */

    public float spawnOffset;//生产时的初始化位置设定
    public bool isTriggerActive = false;

    void Awak()
    {
        if(spawnOffset==0)
        {
            //默认以中心点为模块位置
            Debug.LogError("初始化位置没有设定");
        }
    }

    //模块碰撞时不添加
    void OnTriggerEnter()
    {
        //Debug.Log("1111111111:triggerEnter+"+transform.name);
    }

    void OnTriggerExit()
    {
        //Debug.Log("2222222222222:triggerExit+" + transform.name);
    }
}
