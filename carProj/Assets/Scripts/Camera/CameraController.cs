﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject m_objCube = GameObject.Find("GamePlayer");
        CameraViewTransform cvt = Camera.main.GetComponent<CameraViewTransform>();
        cvt.init(m_objCube.transform);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
