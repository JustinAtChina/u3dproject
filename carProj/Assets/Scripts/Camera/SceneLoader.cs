﻿using UnityEngine;
using System.Collections;
using System.IO;

using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour {

    public string AssetbundleURL;
    public GameObject SceneRoot;
    //public static bool loaded = false;


    void Start()
    {
        Debug.Log("##LoadSence!##");
        AssetbundleURL = "file://" + Application.streamingAssetsPath+"/start0.assetbundle";
       // AssetbundleURL  = "http://cs.101.com/v0.1/static/edu_product/esp/assets/91c1378f-499c-4722-9d68-dfe26b40a88e.pkg/3d_pc-LOD2/51811e637e08475b835bf37e1501195f.assetbundle";
        StartCoroutine(LoadStartSence(AssetbundleURL));
        
        //loaded = true;

    }

    void OnDestroy()
    {
        Resources.UnloadUnusedAssets();
    }


    IEnumerator LoadStartSence(string name)
    {
        WWW www = new WWW(name);
        yield return www;
        AssetBundle bundles = www.assetBundle;
        var objs = bundles.LoadAllAssets<GameObject>();
        foreach (var o in objs)
        {
            GameObject obj = Instantiate(o) as GameObject;
            obj.transform.SetParent(SceneRoot.transform);
        }
        //SceneRoot.transform.position = new Vector3(35,-8,-10);

    }
}
