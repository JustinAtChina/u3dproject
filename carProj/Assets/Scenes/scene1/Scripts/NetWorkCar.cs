﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class NetWorkCar : NetworkBehaviour {

    [SyncVar]
    public Color color;
    [SyncVar]
    public string playerName;

    private NetworkInstanceId playerNetID;

    void Start()
    {
        Renderer[] renderers = GetComponentsInChildren<Renderer>();
        foreach(Renderer ren in renderers)
        {
            ren.material.color = color;
        }
        SetScreenPlayerName();
    }

    public override void OnStartLocalPlayer()
    {
        //Debug.Log("--" + playerName);
        GetNetIdentity();
        SetNetIdentity();
        SetScreenPlayerName();
    }

    void Update()
    {
        
        if(transform.name==""||transform.name=="PlayerInfo(Clone)"||transform.name=="Car(Clone)")
        {
           // Debug.Log("000" + transform.name);
            SetNetIdentity();
        }
    }

    //设置车子的玩家名
    void SetScreenPlayerName()
    {
        foreach(Transform child in gameObject.transform)
        {
            if(child.transform.name=="PlayerNameText")
            {
                child.GetComponent<TextMesh>().text = playerName;
                break;
            }

        }
    }

    void SetNetIdentity()
    {
        Debug.Log("playerName:" + playerName);
        if (playerName == "" || playerName == null)
        {
            transform.name = playerNetID + "";
        }
        else
        {
            transform.name = playerName;
        }

    }

    [Client]
    void GetNetIdentity()
    {
        if(playerName==""||playerName==null)
        {
            playerNetID = GetComponent<NetworkIdentity>().netId;
            CmdTellServerMyIdentity(playerNetID+"");
        }
        else
        {
          //  CmdTellServerMyIdentity(playerName);
        }
    }

    [Command]
    void CmdTellServerMyIdentity(string name)
    {
        playerName = name;
    }

}
