﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class sceneManager : NetworkBehaviour {

    private WheelCollider WheelFLCollider;

	void Start () {
        if(isLocalPlayer)
        {
            GameObject.Find("Main_Camera").GetComponent<SmoothFollow>().targetTransform = transform;
            GetWheelFLCollider();
            GameObject.Find("LabelSpeed").GetComponent<SpeedDisplay>().FLWheelCollider = WheelFLCollider;
        }
       
    }

    void GetWheelFLCollider()
    {
        foreach(Transform child in transform)
        {
            if(child.transform.name== "WheelColliders")
            {
                foreach(Transform t in child)
                {
                    if (t.transform.name == "WheelFLCollider")
                    {
                        WheelFLCollider = t.GetComponent<WheelCollider>();
                        break;
                    }
                }
                break;
            }
        }
    }

}
