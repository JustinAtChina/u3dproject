﻿using UnityEngine;
using System.Collections;

public class CarShadow : MonoBehaviour {

    public Transform carTransform;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.position = carTransform.position + Vector3.up * 8;
        this.transform.localEulerAngles = new Vector3(90, 0, 0);
	}
}
