﻿using UnityEngine;
using System.Collections;

public class SmoothFollow : MonoBehaviour {

    public Transform targetTransform;

    public float height = 3f;
    public float distance = -10f;
    public float smoothSpeed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 targetForward = targetTransform.forward;
        targetForward.y = 0;

        Vector3 currentForward = this.transform.forward;
        currentForward.y = 0;

        Vector3 forward = Vector3.Lerp(currentForward.normalized, targetForward.normalized, smoothSpeed * Time.deltaTime);

        Vector3 targetpos = targetTransform.position + Vector3.up * height + targetTransform.forward * distance;
        this.transform.position = targetpos;
        this.transform.LookAt(targetTransform);
	}
}
