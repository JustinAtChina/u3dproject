﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SpeedDisplay : MonoBehaviour {

    public WheelCollider FLWheelCollider;

    public RectTransform pointSpeed;

    public float carSpeed;

    private Text labelSpeed;
    private float zRotation;

	// Use this for initialization
	void Start () {
        labelSpeed = this.GetComponent<Text>();
        zRotation = pointSpeed.eulerAngles.z;
    }
	
	// Update is called once per frame
	void Update () {
        //print(FLWheelCollider.rpm);
        carSpeed = (FLWheelCollider.rpm/360) * (FLWheelCollider.radius * 2 * Mathf.PI) * 60 / 1000;
        labelSpeed.text = Mathf.Round(carSpeed).ToString();
        if (carSpeed <= 0) {
            carSpeed = 0;
        }
        float newZRotation = zRotation - carSpeed * (270 / 140f);
        pointSpeed.eulerAngles = new Vector3(0,0,newZRotation);
	}
}
