﻿using UnityEngine;
using System.Collections;

public class CarDriving : MonoBehaviour {

    public WheelCollider flWheelCollider;
    public WheelCollider frWheelCollider;
    public WheelCollider rlWheelCollider;
    public WheelCollider rrWheelCollider;

    public Transform flWheelModel;
    public Transform frWheelModel;
    public Transform rlWheelModel;
    public Transform rrWheelModel;
    public Transform flDiscBrake;
    public Transform frDiscBrake;
    public Transform centerOfMass;

    public float motorTorgue = 4000f;
    public float steerAngle = 10f;
    public float maxSpeed = 140f;
    public float minSpeed = 30f;
    public float brakeTorque = 1000f;//刹车力量

    private bool isBreaking = false;

	// Use this for initialization
	void Start () {
        this.GetComponent<Rigidbody>().centerOfMass = centerOfMass.localPosition;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        float currentSpeed = (flWheelCollider.rpm / 360) * (flWheelCollider.radius * 2 * Mathf.PI) * 60 / 1000;

        if ((currentSpeed > 0 && Input.GetAxis("Vertical") < 0) || (currentSpeed < 0 && Input.GetAxis("Vertical") > 0))
        {
            isBreaking = true;
        }
        else {
            isBreaking = false;
        }
        //在规定速度范围内才可以施加动力
        if ((currentSpeed > maxSpeed && Input.GetAxis("Vertical") > 0) || (currentSpeed < - minSpeed && Input.GetAxis("Vertical") < 0)) {
            flWheelCollider.motorTorque = 0;
            frWheelCollider.motorTorque = 0;
        }
        else{
            flWheelCollider.motorTorque = Input.GetAxis("Vertical") * motorTorgue;
            frWheelCollider.motorTorque = Input.GetAxis("Vertical") * motorTorgue;
        }

        if (isBreaking){
            flWheelCollider.motorTorque = 0;
            frWheelCollider.motorTorque = 0;

            flWheelCollider.brakeTorque = brakeTorque;
            frWheelCollider.brakeTorque = brakeTorque;
        }
        else {
            flWheelCollider.brakeTorque = 0;
            frWheelCollider.brakeTorque = 0;
        }

        flWheelCollider.steerAngle = Input.GetAxis("Horizontal") * steerAngle;
        frWheelCollider.steerAngle = Input.GetAxis("Horizontal") * steerAngle;

        RotateWheel();
        SteerWheel();
	}

    //控制轮子转动
    public void RotateWheel() {
        flDiscBrake.Rotate(flWheelCollider.rpm / 60 * Time.deltaTime * Vector3.right);
        frDiscBrake.Rotate(flWheelCollider.rpm / 60 * Time.deltaTime * Vector3.right);
        rlWheelModel.Rotate(flWheelCollider.rpm / 60 * Time.deltaTime * Vector3.right);
        rrWheelModel.Rotate(flWheelCollider.rpm / 60 * Time.deltaTime * Vector3.right);
    }

    //控制轮子转向
    public void SteerWheel() {
        Vector3 localEulerAngles = flWheelModel.localEulerAngles;
        localEulerAngles.y = flWheelCollider.steerAngle;
        flWheelModel.localEulerAngles = localEulerAngles;
        frWheelModel.localEulerAngles = localEulerAngles;
    }
}
